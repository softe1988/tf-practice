# AWS configuration should be set in your configuration file at
# ~/.aws/config

provider "aws" {
  profile = "default"
}

# Pull tfstate from s3
# NOTE: this gets loaded before variables, so they cannot be used here
# change data.config.key to <your_dirname>.tfstate to save file to s3 bucket

data "terraform_remote_state" "tfstate" {
  backend = "s3"
  config {
    bucket = "demo-terraform-states"
    key    = "states/vpc-resources.tfstate"
    region = "us-east-1"
    profile = "default"
  }
}

# Push tfstate from s3
# change terraform.backend.key to <your_dirname>.tfstate to save file to s3 bucket

terraform {
  backend "s3" {
    bucket = "demo-terraform-states"
    key    = "states/vpc-resources.tfstate"
    region = "us-east-1"  #hardcoded on purpose
    profile = "default"
  }
}