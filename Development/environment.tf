/*
data "aws_vpc" "current" {
  id = "${aws_vpc.dev_env.id}"
}
*/


########################
## Subnets
########################

resource "aws_subnet" "dev_subnet_priv" {
  vpc_id                = "${aws_vpc.dev_vpc.id}"
  cidr_block            = "var.dev_cidr_priv"
  availabililty_zone    = "us-east-1a"
  tags {
    Name = "Dev_Private"
  }
}

resource "aws_subnet" "dev_subnet_public" {
  vpc_id                = "${aws_vpc.dev_vpc.id}"
  cidr_block            = "var.dev_cidr_pub"
  availabililty_zone    = "us-east-1f"

  tags {
    Name = "Dev_Public"
  }
}

########################
## Security Groups
########################

# SGs sit inside the subnet to protect ec2 instances 
# Network ACLs protect at the subnet level they are firewalls that sit in front of subnet
resource "aws_security_group" "dev_sg_pub" {
  name = "Public_dev_env_sg"
  description = "allow all inbound traffic"
  vpc_id = "${aws_vpc.dev_vpc.id}"

  // allow https traffic via tcp protocol to ips in the dev_cidr_pub block
  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    #cidr_blocks     = ["${var.dev_cidr_pub}"]
    description = "Dev public https ingress config" 
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["${var.dev_cidr_pub}"]
    description = "Dev public SSH ingress config" 
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"] 
    description = "Dev public https egress config"
  }

  tags {
    Name = "Dev Public"
  }
}

resource "aws_security_group" "dev_sg_priv" {
  name = "Private_dev_env_sg"
  description = "allow specified inbound traffic to private subnet"
  vpc_id = "${aws_vpc.dev_vpc.id}"


  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks     = ["${var.dev_cidr_pub}"]
    description = "Dev public https ingress config" 
  }
  
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["${var.dev_cidr_pub}"]
    description = "Dev public SSH ingress config" 
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"] 
    description = "Dev public https egress config"
  }

  tags {
    Name = "Dev Private"
  }
}

########################
## Route Table
########################


########################
## Elastic IP
########################

resource "aws_eip" "dev_env_eip" {
  instance          = ""
  vpc               = true
}

########################
## NAT Gateway
########################

resource "aws_nat_gateway" "dev_pub_gw" {
  allocation_id       = ""
  subnet_id           = "${aws_subnet.dev_subnet_public.id}"
}

########################
## Internet Gateway
########################

resource "aws_internet_gateway" "dev_igw" {
  vpc_id              = "${aws_vpc.dev_vpc.id}"

  tags {
    Name = "Dev_IGW"
  }
}

