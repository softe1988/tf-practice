/* example variable definition
variable "dev_vpc_id" {
  description = "Dev vpc id"
  default = ""
}
*/

variable "dev_cidr_priv" {
  description = "cidr block private subnet"
}

variable "dev_cidr_pub" {
  description = "cidr block public subnet"
}
